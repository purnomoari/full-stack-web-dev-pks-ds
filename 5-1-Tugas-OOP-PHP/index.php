<?php

//Kelas utama Hewan
abstract class Hewan {
  public $nama;
  public $darah;
  public function __construct($nama , $darah ) {
    $this->name = $nama ;
    $this->$darah;
  }
  
  abstract public function atraksi();
}

class Elang extends Hewan {
  public function atraksi() : string {
    return " $this->name terbang tinggi";
  }
}

class Harimau extends Hewan {
  public function atraksi() {
    return "$this->name lari cepat";
  }
}

//class utama Fight
abstract class Fight{
    public $attackPower;
    public $defencePower;
    public function __construct($attackPower, $defencePower) {
      $this->name = $attackPower;
      $this->name = $defencePower;
    }
    abstract public function serang();
    abstract public function diserang();
    
}
  
  // Child classes
  class Elang extends Fight {
    public function serang()  {
      return "$this->name sedang Menyerang  oleh Harimau";
    }
    public function diserang()  {
        return "$this->name sedang diserang Harimau";
    }
}
  class Harimau extends Fight {
    public function serang() {
      return " $this->name Sedang Menyerang Elang";
    }
    public function diserang() {
        return " $this->name Sedang diserang Elang";
      }
  }
  

interface HewanInterface {
  
    public function getInfoHewan();
  
  }
  
  class Elang implements HewanInterface {
  
    public function getInfoHewan() {
  
       return '';
  
    }
  
  }
  
  class Harimau implements HewanInterface {
  
     public function getInfoHewan() {
  
        return '';
  
     }
  
  }
  

?>