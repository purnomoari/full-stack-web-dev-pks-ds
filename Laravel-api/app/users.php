<?php

namespace App;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $fillable = ['username','email','name','roles_id','password'.'email_verified_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if( empty($model->id)){
                $model->id = Str::uuid();
            }

            $model->roles_id=roles::where('name', 'author')->first()->id;
        });

    }
    public function roles()
    {
        return $this->belongsTo('App\roles');
    }
    public function otp_code()
    {
        return $this->hasIOne('App\OtpCode');
    }
}
