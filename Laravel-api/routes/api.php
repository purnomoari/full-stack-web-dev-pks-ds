<?php


use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource post
 */
Route::apiResource('/posts', 'PostController');
Route::apiResource('/roles', 'RolesController');
Route::apiResource('/comment', 'CommentController');

Route::group([
    'prefix' => 'auth' , 
    'namespace' => 'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('auth.register');
});